public class Baju extends Pakaian{
	//kode warna R=merah, B=Biru, G=Hijau, U=Belum ditentukan
	public char kodeWarna = 'U';
	//method ini menampilkan nilai untuk suatu item
	public void tampilInformasiBaju(){
		System.out.println("ID Baju: "+getID());
		System.out.println("Keterangan: "+getKeterangan());
		System.out.println("Kode Warna: "+kodeWarna);
		System.out.println("Harga baju: "+getHarga());
		System.out.println("Jumlah Stok: "+getJmlStok());
	}
	
	public static void main(String[] args){
		Baju losyong= new Baju();
		losyong.tampilInformasiBaju();
	}
}
