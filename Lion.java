public class Lion{
	private String nama;
	private int usia;
	private double tb;
	private double bb;
	private boolean statusJinak;
		
	public void cetakInformasi(){	
		System.out.println("Singa bernama : " + nama);
		System.out.println("Usia : " + usia);
		System.out.println("Tinggi Badan : " + tb);
		System.out.println("Berat Badan : " + bb);
		System.out.println("Jinak? " + statusJinak);			
	}

	public void setNama(String nm){
		nama = nm;
	}

	public void setUsia(int age){
		usia = age;
	}

	public void setTinggi(double tinggi){
		tb = tinggi;
	}

	public void setBerat(double berat){
		bb = berat;
	}
}