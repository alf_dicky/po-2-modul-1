public abstract class LivingThing{
	public void breath(){
		System.out.println("Living Thing Breathing...");
	}
	public void eat(){
		System.out.println("Living Thing Eating...");	
	}
	/**
	*abstarct method walk
	*kita ingin method ini di-overridden oleh subclasses
	*/
	public abstract void walk();
}
