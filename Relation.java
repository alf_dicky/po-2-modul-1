public interface Relation{
	public static void main(String[] args){
		public boolean isGreater(Object a, Object b);
		public boolean isLess(Object a, Object b);
		public boolean isEqual(Object a, Object b);
	}
}
